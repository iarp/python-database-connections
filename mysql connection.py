import mysql.connector

connection = mysql.connector.connect(
    host='',
    port=3306,
    user='',
    password='',
    buffered=True
)

cursor = connection.cursor()

try:
    cursor.execute('CREATE TABLE large_data (id INT NOT NULL AUTO_INCREMENT, value1 VARCHAR(255), value2 VARCHAR(255)')
except mysql.connector.Error:
    cursor.execute('TRUNCATE TABLE large_data')

connection.commit()

for x in range(100):
    value2 = 'data {}'.format(x)

    cursor.execute('INSERT INTO large_data (value1, value2) VALUES (%s, %s)', (x, value2,))

connection.commit()

"""
    Examples of SELECT's only.

    Access column data by a zero-based indexed column location

    Index:             0       1          2           3
    Columns Returned: id, first_name, last_name, date_of_birth

    Accessing date_of_birth from row: row[3]

"""

print('Loading one row into memory at a time')
cursor.execute('SELECT id, value1, value2 FROM large_data')
while True:
    row = cursor.fetchone()
    if not row:
        break

    print('one row loaded at a time:', row[0], row[1], row[2])

print('Loading ten rows into memory at a time')
cursor.execute('SELECT id, value1, value2 FROM large_data')
while True:
    ten_rows = cursor.fetchmany(10)
    if not ten_rows:
        break

    print('ten rows loaded at a time:', ten_rows)

    # for row in ten_rows:
    #     print(row[0], row[1], row[2])

print('Loading all rows into memory all at once')
cursor.execute('SELECT id, value1, value2 FROM large_data')
for row in cursor.fetchall():
    print('all rows loaded at once, one row:', row[0], row[1], row[2])

