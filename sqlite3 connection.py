import sqlite3
import sys

connection = sqlite3.connect('cache.db')
cursor = connection.cursor()

cursor.execute('DROP TABLE IF EXISTS large_data')
connection.commit()

cursor.execute('CREATE TABLE IF NOT EXISTS large_data (value1 TEXT, value2 TEXT)')

for x in range(15):
    cursor.execute('INSERT INTO large_data (value1, value2) VALUES (?,?)', (x, 'data {}'.format(x),))

connection.commit()

"""
    Examples of SELECT's only.

    Access column data by a zero-based indexed column location

    Index:             0       1          2           3
    Columns Returned: id, first_name, last_name, date_of_birth

    Accessing date_of_birth from row: row[3]

"""

print('Loading one row into memory at a time')
cursor.execute('SELECT value1, value2 FROM large_data')
while True:
    row = cursor.fetchone()
    if not row:
        break
    print('one row at a time:', row[0], row[1])

print('Loading ten rows into memory at a time')
cursor.execute('SELECT value1, value2 FROM large_data')
while True:
    ten_rows = cursor.fetchmany(10)
    if not ten_rows:
        break

    print('ten rows at once:', ten_rows)

    # for row in ten_rows:
    #     print('single row from a batch of ten:', row[0], row[1])

print('Loading all rows into memory all at once')
cursor.execute('SELECT value1, value2 FROM large_data')
for row in cursor.fetchall():
    print(row[0], row[1])
