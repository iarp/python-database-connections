import pyodbc

hostname = ''
username = 'sa'
password = ''
database = ''

connection = pyodbc.connect(
            driver='{SQL Server}',
            server=hostname,
            database=database,
            uid=username,
            pwd=password,
            port=1433
)  # type: pyodbc.Connection

cursor = connection.cursor()  # type: pyodbc.Cursor

try:
    cursor.execute("""CREATE TABLE large_data (
                              id INT PRIMARY KEY NOT NULL IDENTITY(1,1),
                              value1 VARCHAR(255),
                              value2 VARCHAR(255))""")
except pyodbc.ProgrammingError:
    print('large_data table already exists')
    cursor.execute('TRUNCATE TABLE large_data')

connection.commit()

for x in range(100):
    value2 = 'data {}'.format(x)
    cursor.execute('INSERT INTO large_data (value1, value2) VALUES (?,?)', (x, value2))

connection.commit()

"""
    Examples of SELECT's only.

    Access column data by name as if it were an attribute of a class:

    Columns Returned: id, first_name, last_name, date_of_birth

    Accessing date_of_birth from row: row.date_of_birth

    If your column names happen to have spaces, you would need to use:
    getattr(row, 'date of birth')

"""

print('Loading one row into memory at a time')
cursor.execute('SELECT id, value1, value2 FROM large_data')
while True:
    row = cursor.fetchone()
    if not row:
        break

    print('one row loaded at a time:', row.id, row.value1, row.value2)

print('Loading ten rows into memory at a time')
cursor.execute('SELECT id, value1, value2 FROM large_data')
while True:
    ten_rows = cursor.fetchmany(10)
    if not ten_rows:
        break

    print('ten rows loaded at a time:', ten_rows)

    # for row in ten_rows:
    #     print(row.id, row.value1, row.value2)

print('Loading all rows into memory all at once')
cursor.execute('SELECT id, value1, value2 FROM large_data')
for row in cursor.fetchall():
    print('all rows loaded at once, one row:', row.id, row.value1, row.value2)
